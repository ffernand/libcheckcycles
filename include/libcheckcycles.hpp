//
// Created by fernando on 15/09/22.
//

#ifndef LIBCHECKCYCLES_LIBCHECKCYCLES_HPP
#define LIBCHECKCYCLES_LIBCHECKCYCLES_HPP

typedef long long clock_measure_t;

clock_measure_t get_clock_cycles();

#endif //LIBCHECKCYCLES_LIBCHECKCYCLES_HPP
