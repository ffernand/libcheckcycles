#!/usr/bin/python3
import time

from build import checkcycles

print("Getting cycles from GPU")

for i in range(1, 100):
    cycles = checkcycles.get_clock_cycles()
    print(f"Cycle at iteration {i}:", cycles)
    time.sleep(i / 10)
