//
// Created by fernando on 15/09/22.
//

#include "libcheckcycles.hpp"

#ifdef GPU
extern clock_measure_t get_clock_gpu();
#endif


clock_measure_t get_clock_cpu() {
    return 0;
}


clock_measure_t get_clock_cycles() {
#ifdef GPU
    return get_clock_gpu();
#else
    return get_clock_cpu();
#endif
}
