//
// Created by fernando on 15/09/22.
//

#include "libcheckcycles.hpp"

__device__ clock_measure_t clock_at_t = 0;

__global__ void get_clock_device() {
    clock_at_t = clock64();
}

clock_measure_t get_clock_gpu() {
    /**
     * if it returns 0 it is in an error condition
     */
    clock_measure_t current_clock = 0;
    get_clock_device<<<1, 1>>>();
    auto synch_ret = cudaDeviceSynchronize();
    auto copy_ret = cudaMemcpyFromSymbol(&current_clock, clock_at_t, sizeof(clock_measure_t));
    if (synch_ret != cudaSuccess) {
        return -synch_ret;
    } else if (copy_ret != cudaSuccess) {
        return -copy_ret;
    }
    return current_clock;
}